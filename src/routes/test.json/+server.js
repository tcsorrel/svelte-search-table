/** @type {import('./$types').RequestHandler} */
export async function GET() {
	const data = [
		{site: "[python](https://www.python.org)", version: "3.11.2", b: "2"},
		{site: "https://www.nodejs.org", version: "19.7.0", b: "6"},
		{site: "https://www.rust-lang.org", version: "1.67.1", b: "2"},
	]
	return new Response(JSON.stringify(data), {
		status: 200,
		headers: { 'Content-Type': 'application/json' }
	});
}
