import { browser } from "$app/environment";
import { writable } from "svelte/store";

const initialSearches = JSON.parse(
  browser ? window.localStorage.getItem("searches") ?? "{}" : "{}"
);

export const searches = writable(initialSearches);

searches.subscribe((value) => {
  if (browser) {
    window.localStorage.setItem("searches", JSON.stringify(value));
  }
});

const initialColumns = JSON.parse(
  browser ? window.localStorage.getItem("columns") ?? "{}" : "{}"
);

export const columns = writable(initialColumns);

columns.subscribe((value) => {
  if (browser) {
    window.localStorage.setItem("columns", JSON.stringify(value));
  }
});
